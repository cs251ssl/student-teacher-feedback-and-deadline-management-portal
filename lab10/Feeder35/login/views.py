from django.shortcuts import render
from login.models import logindatabase
from django.contrib.auth import authenticate, login
from forms import loginform,UserForm,UserProfileForm
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth import authenticate, login,logout
from adminuser.models import Student,Courses,Feedbacks,Assignment
from adminuser.forms import CourseProfileForm,feedbackform,student
from django.forms import formset_factory

def registerpage(request):
    if request.user.is_authenticated:
        return HttpResponseRedirect('/adminuser')
    else:
        active={'logintab':False,'registertab':True}
        registered=""
        if request.method == 'POST':
            register=UserForm(request.POST)
            registerprofile=UserProfileForm(request.POST)
            if register.is_valid() and registerprofile.is_valid():
                newuser=register.save(commit=False)
                newuser.set_password(register.cleaned_data['password'])
                newuser.email=newuser.username
                newuser.save()
                newuser_extra=registerprofile.save(commit=False)
                newuser_extra.user=newuser
                newuser_extra.save()
                register=UserForm()
                registerprofile=UserProfileForm()
                loginformobject=loginform()
                registered="You have been Registered . Please check your mail"
                return render(request,'loginpage.html',{'register':register,'registerprofile':registerprofile,'loginformobject':loginformobject,'active':active,'registered':registered})
        else:
            register=UserForm()
            registerprofile=UserProfileForm()
        loginformobject=loginform()
        return render(request,'loginpage.html',{'register':register,'registerprofile':registerprofile,'loginformobject':loginformobject,'active':active,'registered':registered})


def loginpage(request):
    if request.user.is_authenticated:
        return HttpResponseRedirect('/adminuser')
    else:
        active={'logintab':True,'registertab':False}
        accesserror=''
        if request.method == 'POST':
            loginformobject=loginform(request.POST)
            if loginformobject.is_valid():
                useraccess = authenticate(username=loginformobject.cleaned_data['username'],password=loginformobject.cleaned_data['loginpassword'])
                if useraccess:
                    if useraccess.is_active:
                        login(request, useraccess)
                        if useraccess.is_staff:
                            return HttpResponseRedirect('/adminuser')
                        else:
                            return HttpResponse('Instructor logged in site under construction')
                    else:
                        accesserror="Your account is disabled.Please contact administrator"
                        return HttpResponseRedirect("/login")
                else:
                    if accesserror == '':
                        accesserror='*Please enter valid Username and Password'
        else:
            loginformobject=loginform()
        register=UserForm()
        registerprofile=UserProfileForm()
        return render(request,'loginpage.html',{'register':register,'registerprofile':registerprofile,'loginformobject':loginformobject,'accesserror':accesserror,'active':active})

            


  
