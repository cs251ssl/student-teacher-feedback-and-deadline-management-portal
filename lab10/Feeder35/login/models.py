from django.db import models
from django.contrib.auth.models import User
from django.core.validators  import RegexValidator

Gender_choices = (("MALE", "Male"),("FEMALE", "Female"),)
DATE_INPUT_FORMATS = ('%d-%m-%Y','%Y-%m-%d')

class logindatabase(models.Model):
    user = models.OneToOneField(User,on_delete=models.CASCADE,primary_key=True,)
    Gender=models.CharField(max_length = 10,choices=Gender_choices,default="Male")
    Birthdate = models.DateField()
